branch="release/25.04"
reposdir="/home/tsdgeos/devel/kde"

cat modules.git | while read a b; do
	echo $a
	pushd $reposdir/$a || exit 1
	git checkout master || exit 3
	git pull --rebase || exit 4
	diff=`git diff origin/master`
	if [ -n "$diff" ]; then
		exit 7
	fi
	git push origin master || exit 5
	git push origin master:$branch || exit 2
	git checkout $branch || exit 8
	git pull --rebase || exit 8
	git checkout master || exit 9
	popd
done
rm modules.git
