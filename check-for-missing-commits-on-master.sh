branch="release/24.12"
reposdir="/home/tsdgeos/devel/kde"

echo "For repos that are flagged here"
echo "  git checkout master && git pull --rebase && git merge origin/release/24.05"
echo "  git diff origin/master -- . :^po"
echo ""
echo "If the changes are only in i18n stuff desktop/appdata or in CMakeLists.txt versions then it's fine"
echo "To go back to your proper master just"
echo "  git reset --hard origin/master"
echo ""
echo ""
echo "Repos that need checking:"
cat modules.git | while read a b; do
    cd $reposdir/$a
    git fetch &> /dev/null
    log=`git log --format=fuller origin/$branch ^origin/master --no-merges --invert-grep --grep=GIT_SILENT --invert-grep --grep=SVN_SILENT --invert-grep --grep="cherry picked from commit"`
    if [ -n "$log" ]; then
        echo "    $a"
    fi
done
rm modules.git
